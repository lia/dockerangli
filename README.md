# dockerAngLI

Inspired from: 
* https://gitlab.cern.ch/CxAODFramework/dockerfiles/-/tree/master 
* https://gitlab.cern.ch/atlas-physics-office/gitlab-integration
* https://gitlab.cern.ch/ATLAS-EGamma/Publications/PERF-2017-01

Pipeline can be created for instance for my thesis repository: https://gitlab.cern.ch/lia
